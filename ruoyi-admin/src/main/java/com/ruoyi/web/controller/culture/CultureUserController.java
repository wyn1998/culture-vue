package com.ruoyi.web.controller.culture;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.culture.domain.CreativePublish;
import com.ruoyi.culture.domain.RegisterInfo;
import com.ruoyi.culture.domain.RegisterPart;
import com.ruoyi.culture.service.FailPassService;
import com.ruoyi.culture.service.RegisterInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ruoyi.common.core.domain.AjaxResult.success;

@RestController
@RequestMapping("/cultureUser")
@Api(tags = {"文创用户相关控制器"})
public class CultureUserController extends BaseController {


    @Autowired
    RegisterInfoService registerInfoService;

    @Autowired
    FailPassService failPassService;

//    @PostMapping("/register")
//    @ApiOperation("文创用户信息注册")
//    public AjaxResult addOne(@RequestBody RegisterPart registerPart) {
//        registerInfoService.insert(registerPart);
//        return success();
//    }


    @GetMapping("/failPass/list")
    @ApiOperation("已注册用户展示")
    public TableDataInfo failPassList(@ApiParam(value = "身份证号") @RequestParam(value = "identity",required = false)String identity,
    @ApiParam(value = "账号") @RequestParam(value = "username",required = false)String username) {
        startPage();
        List<RegisterInfo> userList = failPassService.getUserList(identity,username);
        return getDataTable(userList);
    }


    @GetMapping("/failPass")
    @ApiOperation("已注册用户查询注册信息是否通过")
    public AjaxResult failPass(@ApiParam(value = "身份证号") @RequestParam(value = "identity")String identity) {
        String delFlag = failPassService.selectUserNameByIdentity(identity);
        return success(delFlag);
    }


    @PutMapping("/failPass/edit")
    @ApiOperation("已注册未通过用户后台审核成为会员")
    public AjaxResult failPassUpdate(@ApiParam(value = "账号") @RequestParam(value = "username")String username,
                                     @ApiParam(value = "状态") @RequestParam(value = "deFlag")String deFlag) {
        return success(failPassService.updateFailPass(username,deFlag));
    }

    @GetMapping("/info/check")
    @ApiOperation("用户注册校验")
    public AjaxResult check(@ApiParam(value = "账号") @RequestParam(value = "username")String username,
                            @ApiParam(value = "身份证号") @RequestParam(value = "identity")String identity) {
        boolean check = failPassService.check(username, identity);
        return success(check);
    }


    @PutMapping("/info/update")
    @ApiOperation("文创用户信息修改")
    public AjaxResult update(@RequestBody RegisterPart registerPart) {
        registerInfoService.update(registerPart);
        return success();
    }

    @DeleteMapping("/info/delete")
    @ApiOperation("用户删除")
    public AjaxResult delete(@RequestParam String username) {
        Integer delete = registerInfoService.delete(username);
        return success(delete);
    }


    @GetMapping("/info/getOne")
    @ApiOperation("文创用户信息查询")
    public AjaxResult getOne(@RequestParam String username) {
        RegisterInfo registerInfo = registerInfoService.searchOne(username);
        return success(registerInfo);
    }

    @GetMapping("/info/works")
    @ApiOperation("用户个人作品查询")
    public AjaxResult workList(@RequestParam Long userId) {
        List<CreativePublish> works = registerInfoService.workList(userId);
        return success(works);
    }

}
