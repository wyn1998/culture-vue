package com.ruoyi.web.controller.culture;


import com.ruoyi.common.core.controller.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/enterpriseHall")
@Api(tags = {"企业展厅控制器"})
public class EnterpriseHallController extends BaseController {
}
