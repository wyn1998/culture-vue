package com.ruoyi.web.controller.system;



import com.ruoyi.culture.domain.RegisterPart;
import com.ruoyi.culture.service.RegisterInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.framework.web.service.SysRegisterService;
import com.ruoyi.system.service.ISysConfigService;

/**
 * 注册验证
 * 
 * @author ruoyi
 */
@RestController
@Api(tags = {"注册控制器"})
public class SysRegisterController extends BaseController
{
    @Autowired
    private SysRegisterService registerService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    RegisterInfoService registerInfoService;

    @PostMapping("/register")
    @ApiOperation("注册接口")
    public AjaxResult register(@RequestBody RegisterPart user)
    {
//        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
//        {
//            return error("当前系统没有开启注册功能！");
//        }
        String msg = registerService.register(user.getRegisterBody());

        user.setUserName(user.getRegisterBody().getUsername());
        registerInfoService.insert(user);
        return success(msg);
    }
}
