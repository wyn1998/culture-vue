package com.ruoyi.web.controller.culture;

import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.culture.domain.Classification;
import com.ruoyi.culture.domain.CreativePublish;
import com.ruoyi.culture.domain.Label;
import com.ruoyi.culture.service.ClassificationService;
import com.ruoyi.culture.service.CreativePublishService;
import com.ruoyi.culture.service.LabelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.ruoyi.common.core.domain.AjaxResult.success;
import static com.ruoyi.common.utils.PageUtils.startPage;

@RestController
@RequestMapping("/creativeWork")
@Api(tags = {"文创作品在线发布控制器"})
public class CreativeWorkController extends BaseController {

    @Autowired
    CreativePublishService creativePublishService;


    @Autowired
    ClassificationService classificationService;


    @Autowired
    LabelService labelService;

    @PostMapping("/collect")
    @ApiOperation("文创活动信息发布")
    public AjaxResult collect(@RequestBody CreativePublish creativePublish) {
        Integer insert = creativePublishService.insert(creativePublish);
        return success(insert);
    }

    @GetMapping("/creative")
    @ApiOperation("文创活动信息展示")
    public TableDataInfo show(@ApiParam(value = "标题") @RequestParam(value = "title",required = false) String title,
                              @ApiParam(value = "类型id") @RequestParam(value = "categoryId",required = false) Integer categoryId,
                              @ApiParam(value = "标签id") @RequestParam(value = "labelId",required = false) Integer labelId,
                              @ApiParam(value = "0为文创作品，2为活动报名") @RequestParam(value = "type",required = false) String type,
                              @ApiParam(value = "开始日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date beginningDate,
                              @ApiParam(value = "截至日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endingDate

    ) {
        startPage();
        List<CreativePublish> list = creativePublishService.list(title, categoryId, labelId,type, beginningDate, endingDate);
        List<Map<String, Object>> creativePublishList = creativePublishService.getMaps(list);
        long total = new PageInfo<>(list).getTotal();
        return getDataTable2(creativePublishList,total);
    }
    @GetMapping("/creatives")
    @ApiOperation("文创活动信息展示")
    public TableDataInfo shows(@ApiParam(value = "标题") @RequestParam(value = "title",required = false) String title,
                              @ApiParam(value = "类型id") @RequestParam(value = "categoryId",required = false) Integer categoryId,
                              @ApiParam(value = "标签id") @RequestParam(value = "labelId",required = false) Integer labelId,
                              @ApiParam(value = "0为文创作品，2为活动报名") @RequestParam(value = "type",required = false) String type,
                              @ApiParam(value = "开始日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date beginningDate,
                              @ApiParam(value = "截至日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endingDate

    ) {
        return getDataTable2( creativePublishService.getMap(title, categoryId, labelId,type, beginningDate, endingDate));
    }


    @GetMapping("/creative/details")
    @ApiOperation("文创活动信息详情展示")
    public AjaxResult showDetails(@ApiParam(value = "id") Integer id,
                              @ApiParam(value = "0为文创作品，2为活动报名") String type) {


        return  creativePublishService.getDetails(id,type);
    }


    @PutMapping("/update")
    @ApiOperation("文创活动信息修改")
    public AjaxResult update(@RequestBody CreativePublish creativePublish) {
        creativePublishService.update(creativePublish);
        return success();
    }


    @DeleteMapping("/delete")
    @ApiOperation("文创活动信息删除")
    public AjaxResult delete(@ApiParam(value = "id") @RequestParam(value = "id") Integer id) {
        creativePublishService.delete(id);
        return success();
    }



    @PostMapping("/type/add")
    @ApiOperation("新增作品分类")
    public AjaxResult addType(@RequestBody Classification classification) {
        classificationService.insert(classification);
        return success();
    }

    @GetMapping("/type/list")
    @ApiOperation("作品分类下拉选展示")
    public AjaxResult typeList() {
        List<Classification> list = classificationService.list();
        return success(classificationService.buildDeptTree(list));
    }

    @PutMapping("/type/update")
    @ApiOperation("修改作品分类")
    public AjaxResult updateType(@RequestBody Classification classification) {
        classificationService.update(classification);
        return success();
    }

    @DeleteMapping("/type/delete")
    @ApiOperation("删除作品分类")
    public AjaxResult deleteType(@ApiParam(value = "id") @RequestParam(value = "id") Integer id){
        classificationService.delete(id);
        return success();
    }

    @GetMapping("/type/One")
    @ApiOperation("查询单个作品分类")
    public AjaxResult getTypeOne(@ApiParam(value = "id") @RequestParam(value = "id") Integer id){
        return success(classificationService.getOne(id));
    }

    @PostMapping("/label/add")
    @ApiOperation("新增作品标签")
    public AjaxResult addLabel(@RequestBody Label label) {
      Integer insert =  labelService.insert(label);
        return success(insert);
    }

    @GetMapping("/label/list")
    @ApiOperation("展示标签")
    public TableDataInfo labelList() {
        List<Label> list = labelService.list();
        return getDataTable(list);
    }

    @GetMapping("/label/getOne")
    @ApiOperation("查询单个标签")
    public AjaxResult labelOne(@ApiParam(value = "id") @RequestParam(value = "id",required = false) Integer id) {
        return success(labelService.getOne(id));
    }

    @PutMapping("/label/update")
    @ApiOperation("修改标签")
    public AjaxResult updateLabel(@RequestBody Label label) {
        labelService.update(label);
        return success();
    }


    @DeleteMapping("/label/delete")
    @ApiOperation("删除标签")
    public AjaxResult deleteLabel(@ApiParam(value = "id") @RequestParam(value = "id",required = false) Integer id){
        labelService.delete(id);
        return success();
    }

}
