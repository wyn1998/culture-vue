package com.ruoyi.web.controller.culture;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.culture.domain.VoteInfo;
import com.ruoyi.culture.service.VoteInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/votingStatistics")
@Api(tags = {"投票统计控制器"})
public class VotingStatisticsController extends BaseController {


    @Autowired
    VoteInfoService voteInfoService;

    @PostMapping("/add")
    @ApiOperation("投票信息新增")
    public AjaxResult add(@RequestBody VoteInfo voteInfo) {
        voteInfoService.add(voteInfo);
        return success();
    }

    @GetMapping("/list")
    @ApiOperation("投票信息展示")
    public AjaxResult list() {
        return success(voteInfoService.list());
    }


    @PutMapping("/update")
    @ApiOperation("投票信息修改")
    public AjaxResult update(@RequestBody VoteInfo voteInfo) {
        voteInfoService.update(voteInfo);
        return success();
    }

    @DeleteMapping("/delete")
    @ApiOperation("投票信息删除")
    public AjaxResult delete(@ApiParam(value = "id") @RequestParam(value = "id") Integer id) {
        voteInfoService.delete(id);
        return success();
    }

    @GetMapping("/statistics")
    @ApiOperation("统计接口")
    public AjaxResult statistics(@ApiParam(value = "产品id集合") @RequestParam(value = "id")List<Integer> ids) {
        return success( voteInfoService.statistics(ids));
    }
}
