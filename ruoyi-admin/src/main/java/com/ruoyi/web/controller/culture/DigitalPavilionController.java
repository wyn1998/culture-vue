package com.ruoyi.web.controller.culture;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.culture.domain.ArtistsInfo;
import com.ruoyi.culture.domain.ExhibitionInfo;
import com.ruoyi.culture.service.ArtistsInfoService;
import com.ruoyi.culture.service.ExhibitionInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/digitalPavilion")
@Api(tags = {"数字展馆控制器"})
public class DigitalPavilionController extends BaseController {

    @Autowired
    ArtistsInfoService artistsInfoService;

    @Autowired
    ExhibitionInfoService exhibitionInfoService;


    @PostMapping("/artists")
    @ApiOperation("艺术家信息新增")
    public AjaxResult add(@RequestBody ArtistsInfo artistsInfo) {
        artistsInfoService.insert(artistsInfo);
        return success();
    }

    @GetMapping("/artists")
    @ApiOperation("艺术家信息展示")
    public AjaxResult list() {
        return success(artistsInfoService.list());
    }


    @PutMapping("/artists")
    @ApiOperation("艺术家信息修改")
    public AjaxResult update(@RequestBody ArtistsInfo artistsInfo) {
        artistsInfoService.update(artistsInfo);
        return success();
    }

    @DeleteMapping("/artists")
    @ApiOperation("艺术家信息删除")
    public AjaxResult delete(@ApiParam(value = "id") @RequestParam(value = "id") Integer id) {
        artistsInfoService.delete(id);
        return success();
    }


    @PostMapping("/exhibition")
    @ApiOperation("展览资讯新增")
    public AjaxResult add(@RequestBody ExhibitionInfo exhibitionInfo) {
        exhibitionInfoService.insert(exhibitionInfo);
        return success();
    }

    @GetMapping("/exhibition")
    @ApiOperation("展览资讯展示")
    public AjaxResult listExhibition() {
        return success( exhibitionInfoService.list());
    }


    @PutMapping("/exhibition")
    @ApiOperation("展览资讯修改")
    public AjaxResult update(@RequestBody ExhibitionInfo artistsInfo) {
        exhibitionInfoService.update(artistsInfo);
        return success();
    }

    @DeleteMapping("/exhibition")
    @ApiOperation("展览资讯删除")
    public AjaxResult deleteExhibition(@ApiParam(value = "id") @RequestParam(value = "id") Integer id) {
        exhibitionInfoService.delete(id);
        return success();
    }


}
