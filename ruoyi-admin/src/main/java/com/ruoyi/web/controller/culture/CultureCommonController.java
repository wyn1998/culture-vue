package com.ruoyi.web.controller.culture;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.culture.domain.Picture;
import com.ruoyi.culture.service.PictureService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Api(tags = {"图片关联数据库接口"})
@RequestMapping("/culture/common")
public class CultureCommonController extends BaseController {


    @Autowired
    PictureService pictureService;


    @PostMapping("/picture")
    @ApiOperation("上传图片地址保存到数据库")
    public AjaxResult addOne(@RequestBody List<Picture> pictures) {
        for (Picture picture : pictures) {
        pictureService.insert(picture);
        }
        return success();
    }

    @GetMapping("/picture")
    @ApiOperation("查询数据库中的图片地址信息")
    public AjaxResult getAddress(@ApiParam(value = "0为文创活动，1为文创会议，2为展览咨询") @RequestParam(value = "type") String type,
                                 @ApiParam(value = "对应表格id") @RequestParam(value = "typeFormId") Long typeFormId){
        AjaxResult list = pictureService.getList(type, typeFormId);
        return success(list);
    }


    @DeleteMapping("/picture/delete")
    @ApiOperation("删除数据库中的图片地址信息")
    public AjaxResult deleteAddress(@ApiParam(value = "0为文创活动，1为文创会议，2为展览咨询") @RequestParam(value = "type") String type,
                                 @ApiParam(value = "对应表格id") @RequestParam(value = "typeFormId") Long typeFormId){
        pictureService.delete(type, typeFormId);
        return success();
    }

}
