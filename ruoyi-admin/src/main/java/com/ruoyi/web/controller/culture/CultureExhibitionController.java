package com.ruoyi.web.controller.culture;

import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.culture.domain.CultureActivityApplication;
import com.ruoyi.culture.domain.CultureMeet;
import com.ruoyi.culture.service.CultureActivityApplicationService;
import com.ruoyi.culture.service.CultureMeetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.ruoyi.common.core.domain.AjaxResult.success;
import static com.ruoyi.common.utils.PageUtils.startPage;

@RestController
@Api(tags = {"文化展成控制器"})
@RequestMapping("/cultureExhibition")
public class CultureExhibitionController extends BaseController {

    @Autowired
    CultureMeetService cultureMeetService;

    @Autowired
    CultureActivityApplicationService cultureActivityApplicationService;

    @PostMapping("/meet/collect")
    @ApiOperation("会议信息发布")
    public AjaxResult collect(@RequestBody CultureMeet cultureMeet) {
        return success(cultureMeetService.insert(cultureMeet));
    }

    @PutMapping("/meet/edit")
    @ApiOperation("会议信息修改")
    public AjaxResult meetEdit(@RequestBody CultureMeet cultureMeet) {
        cultureMeetService.edit(cultureMeet);
        return success();
    }


    @GetMapping("/meets")
    @ApiOperation("会议信息展示列表")
    public TableDataInfo show(@ApiParam(value = "标题") @RequestParam(value = "title", required = false) String title,
                              @ApiParam(value = "开始日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date beginningDate,
                              @ApiParam(value = "截至日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endingDate
    ) {
//        startPage();
        return getDataTable(cultureMeetService.getLists(title, beginningDate, endingDate));
    }

    @GetMapping("/meet")
    @ApiOperation("会议信息展示列表")
    public TableDataInfo showMeet(@ApiParam(value = "标题") @RequestParam(value = "title", required = false) String title,
                                  @ApiParam(value = "开始日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date beginningDate,
                                  @ApiParam(value = "截至日期") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endingDate
    ) {
        startPage();
        List<CultureMeet> list = cultureMeetService.list(title, beginningDate, endingDate);
        List<Map<String, Object>> CultureMeetList = cultureMeetService.getList(list);
        long total = new PageInfo<>(list).getTotal();
        return getDataTable2(CultureMeetList, total);
    }

    @GetMapping("/meet/details")
    @ApiOperation("会议信息展示详情")
    public AjaxResult show(@ApiParam(value = "id") @RequestParam(value = "id") Integer id) {
        return cultureMeetService.getDetails(id);
    }


    @DeleteMapping("/meet/delete")
    @ApiOperation("会议信息删除")
    public AjaxResult deleteMeet(@ApiParam(value = "id") @RequestParam(value = "id") Integer id) {
        cultureMeetService.delete(id);
        return success();
    }

    @PostMapping("/activity/signUp")
    @ApiOperation("文创活动报名")
    public AjaxResult signUp(@RequestBody CultureActivityApplication cultureActivityApplication) {
        cultureActivityApplicationService.signUp(cultureActivityApplication);
        return success();
    }

}
