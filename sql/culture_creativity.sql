/*
 Navicat Premium Data Transfer

 Source Server         : 172.16.60.39_3306
 Source Server Type    : MySQL
 Source Server Version : 80300
 Source Host           : 172.16.60.39:3306
 Source Schema         : culture_creativity

 Target Server Type    : MySQL
 Target Server Version : 80300
 File Encoding         : 65001

 Date: 24/04/2024 08:51:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for artists_info
-- ----------------------------
DROP TABLE IF EXISTS `artists_info`;
CREATE TABLE `artists_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `artists_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '艺术家姓名',
  `artists_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '简介',
  `work_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '作品链接',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of artists_info
-- ----------------------------

-- ----------------------------
-- Table structure for classification
-- ----------------------------
DROP TABLE IF EXISTS `classification`;
CREATE TABLE `classification`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` bigint NOT NULL COMMENT '父级id',
  `ancestor_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '祖级id',
  `classification_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '发布分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classification
-- ----------------------------
INSERT INTO `classification` VALUES (1, 0, '0', '文创信息类别', '2024-03-22 14:47:55', '2024-03-22 14:47:55', NULL);
INSERT INTO `classification` VALUES (2, 1, '0,1', '作品', '2024-03-22 15:00:18', '2024-03-22 15:00:18', NULL);
INSERT INTO `classification` VALUES (3, 2, '0,1,2', '影视作品', '2024-03-22 15:02:42', '2024-03-22 15:02:42', NULL);
INSERT INTO `classification` VALUES (4, 1, '0,1', '新闻', '2024-03-25 14:50:06', '2024-03-25 14:50:06', NULL);
INSERT INTO `classification` VALUES (5, 1, '0,1', '照片', '2024-03-25 14:50:39', '2024-03-25 14:50:39', NULL);
INSERT INTO `classification` VALUES (6, 1, '0,1', '图画', '2024-03-25 14:50:45', '2024-03-25 14:50:45', NULL);
INSERT INTO `classification` VALUES (7, 6, '0,1,6', '水彩画', '2024-03-25 14:50:56', '2024-03-25 15:21:03', NULL);

-- ----------------------------
-- Table structure for creative_publish
-- ----------------------------
DROP TABLE IF EXISTS `creative_publish`;
CREATE TABLE `creative_publish`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category_id` bigint NOT NULL COMMENT '分类id',
  `label_id` bigint NULL DEFAULT NULL COMMENT '标签id',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '0为文创作品，2为活动报名',
  `picture_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片表id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文创发布表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of creative_publish
-- ----------------------------
INSERT INTO `creative_publish` VALUES (1, 0, NULL, 'string', 'string', '2024-03-22 19:40:22', '2024-03-22 19:40:22', 'string');

-- ----------------------------
-- Table structure for culture_activity_application
-- ----------------------------
DROP TABLE IF EXISTS `culture_activity_application`;
CREATE TABLE `culture_activity_application`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '报名id',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `creative_publish_id` bigint NOT NULL COMMENT '信息发布id',
  `join_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '活动时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文创活动报名' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of culture_activity_application
-- ----------------------------
INSERT INTO `culture_activity_application` VALUES (1, 1, 1, '2024-03-27 14:32:22', '2024-03-27 09:18:07', '2024-03-27 09:18:07', 'string');

-- ----------------------------
-- Table structure for culture_meet
-- ----------------------------
DROP TABLE IF EXISTS `culture_meet`;
CREATE TABLE `culture_meet`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `picture_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片表id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of culture_meet
-- ----------------------------
INSERT INTO `culture_meet` VALUES (1, 'string2', 'string2', '2024-03-22 12:15:15', '2024-03-22 12:15:15', 'string2');

-- ----------------------------
-- Table structure for exhibition_info
-- ----------------------------
DROP TABLE IF EXISTS `exhibition_info`;
CREATE TABLE `exhibition_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `organization` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属机构',
  `picture_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片表id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exhibition_info
-- ----------------------------

-- ----------------------------
-- Table structure for label
-- ----------------------------
DROP TABLE IF EXISTS `label`;
CREATE TABLE `label`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `label_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标签名称',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of label
-- ----------------------------
INSERT INTO `label` VALUES (1, 'abc', 'efg', '2024-03-25 15:12:23', '2024-03-25 15:25:29', 'string');

-- ----------------------------
-- Table structure for register_info
-- ----------------------------
DROP TABLE IF EXISTS `register_info`;
CREATE TABLE `register_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户id',
  `identity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '身份证号码',
  `register_type` bigint NOT NULL COMMENT '注册类型',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '住址',
  `license` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '企业有效营业执照',
  `enterprise_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '企业信息',
  `legal_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '法人信息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phonenumber` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '注册信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of register_info
-- ----------------------------
INSERT INTO `register_info` VALUES (2, 'hgjjh', 'string', 0, 'string', 'string', 'string', 'string', '2024-03-21 23:05:22', '2024-03-21 23:05:22', 'string', NULL, NULL);
INSERT INTO `register_info` VALUES (3, '11111', '321121199807285910', 2, '镇江', 'aaaaaaa', NULL, NULL, '2024-03-25 16:23:21', '2024-04-02 14:10:15', '修改', '333@.com', '13123123123');

-- ----------------------------
-- Table structure for vote_info
-- ----------------------------
DROP TABLE IF EXISTS `vote_info`;
CREATE TABLE `vote_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `vote_obj_id` bigint NOT NULL COMMENT '投票对象id',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `vote_obj_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '投票对象名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vote_info
-- ----------------------------
INSERT INTO `vote_info` VALUES (1, 2, 3, 'string', '2024-03-29 14:55:49', '2024-03-29 14:55:49', 'string');
INSERT INTO `vote_info` VALUES (2, 3, 3, 'string', '2024-03-29 14:55:55', '2024-03-29 14:55:55', 'string');
INSERT INTO `vote_info` VALUES (3, 4, 3, 'string', '2024-03-29 14:55:59', '2024-03-29 14:55:59', 'string');
INSERT INTO `vote_info` VALUES (4, 5, 3, 'string', '2024-03-29 14:56:03', '2024-03-29 14:56:03', 'string');
INSERT INTO `vote_info` VALUES (5, 5, 2, 'string', '2024-03-29 14:56:09', '2024-03-29 14:56:09', 'string');
INSERT INTO `vote_info` VALUES (6, 5, 1, 'string', '2024-03-29 14:56:11', '2024-03-29 14:56:11', 'string');

SET FOREIGN_KEY_CHECKS = 1;


DROP TABLE IF EXISTS `picture`;
CREATE TABLE `picture`  (
                              `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
                              `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '0为文创活动，1为文创会议，2为展览咨询',
                              `type_form_id` bigint NOT NULL  COMMENT '对应表格id',
                              `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片地址',
                              `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                              `Reversed` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '保留字段',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;