package com.ruoyi.culture.service;


import com.ruoyi.culture.domain.Label;
import com.ruoyi.culture.mapper.LabelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LabelService {

    @Autowired
    LabelMapper labelMapper;

    public Integer insert(Label label) {
        int insertResult = labelMapper.insert(label);
        if (insertResult==0){
            throw new RuntimeException("新增标签失败");
        }
        return label.getId();
    }

    public List<Label> list() {
        List<Label> list = labelMapper.list();
        return list;
    }

    public void update(Label label) {
        label.setModifiedTime(new Date());
        int updateResult = labelMapper.update(label);
        if (updateResult==0){
            throw new RuntimeException("修改标签失败");
        }
    }


    public void delete(int id) {
        int deleteResult = labelMapper.delete(id);
        if (deleteResult==0){
            throw new RuntimeException("删除标签失败");
        }
    }

    public Label getOne(Integer id) {
       return labelMapper.getOne(id);
    }
}
