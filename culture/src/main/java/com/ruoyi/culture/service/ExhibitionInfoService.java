package com.ruoyi.culture.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.culture.domain.CreativePublish;
import com.ruoyi.culture.domain.ExhibitionInfo;
import com.ruoyi.culture.mapper.ExhibitionInfoMapper;
import com.ruoyi.culture.mapper.PictureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class ExhibitionInfoService {

    @Autowired
    ExhibitionInfoMapper exhibitionInfoMapper;

    @Autowired
    PictureMapper pictureMapper;

    public void insert(ExhibitionInfo exhibitionInfo) {
        int insertResult = exhibitionInfoMapper.insert(exhibitionInfo);
        if (insertResult==0){
            throw new RuntimeException("新增展览资讯失败");
        }
    }

    public List<Map<String,Object>> list() {
        List<ExhibitionInfo> list = exhibitionInfoMapper.list();
        List<Map<String,Object>> exhibitionInfoList =new ArrayList<>();
        for (ExhibitionInfo exhibitionInfo : list) {
            Map<String,Object> map = new HashMap<>();
            String pictureId = exhibitionInfo.getPictureId();
            List<Integer> numberList = new ArrayList<>();
            try {
                ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                JsonNode rootNode = mapper.readTree(pictureId); // 解析字符串为JsonNode

                // 遍历JsonNode中的元素，并将它们添加到List中
                for (JsonNode node : rootNode) {
                    numberList.add(node.asInt()); // 将每个元素转换为Integer并添加到List中
                }

            } catch (IOException e) {
                e.printStackTrace(); // 处理任何可能发生的IOException
            }

            List<String> locationsList = new ArrayList<>();
            for (Integer i : numberList) {
                String location = pictureMapper.getLocation(i);
                locationsList.add(location);
            }
            map.put("exhibitionInfo",exhibitionInfo);
            map.put("locationsList",locationsList);

            exhibitionInfoList.add(map);
        }
        return exhibitionInfoList;
    }

    public void update(ExhibitionInfo artistsInfo) {
        artistsInfo.setModifiedTime(new Date());
        int updateResult = exhibitionInfoMapper.update(artistsInfo);
        if (updateResult==0){
            throw new RuntimeException("修改展览资讯失败");
        }
    }

    public void delete(int id) {
        int deleteResult = exhibitionInfoMapper.delete(id);
        if (deleteResult==0){
            throw new RuntimeException("删除展览资讯失败");
        }
    }
}
