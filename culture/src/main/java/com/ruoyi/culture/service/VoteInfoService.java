package com.ruoyi.culture.service;


import com.ruoyi.culture.domain.VoteInfo;
import com.ruoyi.culture.mapper.VoteInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VoteInfoService {

    @Autowired
    VoteInfoMapper voteInfoMapper;

    public void add(VoteInfo voteInfo) {
        int insertResult = voteInfoMapper.insert(voteInfo);
        if (insertResult == 0) {
            throw new RuntimeException("新增投票信息失败");
        }
    }

    public List<VoteInfo> list() {
        List<VoteInfo> list = voteInfoMapper.list();
        return list;
    }

    public void update(VoteInfo voteInfo) {
        voteInfo.setModifiedTime(new Date());
        int updateResult = voteInfoMapper.update(voteInfo);
        if (updateResult == 0) {
            throw new RuntimeException("修改投票信息失败");
        }
    }

    public void delete(int id) {
        int deleteResult = voteInfoMapper.delete(id);
        if (deleteResult == 0) {
            throw new RuntimeException("删除投票信息失败");
        }
    }

    public Map<Integer,Integer> statistics(List<Integer> ids) {
        Map<Integer,Integer> map =new HashMap<>();
        for (Integer id : ids) {
            Integer sum = voteInfoMapper.sum(id);
            map.put(id,sum);
        }
        return map;
    }
}
