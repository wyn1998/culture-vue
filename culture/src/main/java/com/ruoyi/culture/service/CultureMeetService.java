package com.ruoyi.culture.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.culture.domain.CultureMeet;
import com.ruoyi.culture.mapper.CultureMeetMapper;
import com.ruoyi.culture.mapper.PictureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class CultureMeetService {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    CultureMeetMapper cultureMeetMapper;

    @Autowired
    PictureMapper pictureMapper;

    public Integer insert(CultureMeet cultureMeet) {
        int result = cultureMeetMapper.addOne(cultureMeet);
        if (result == 0) {
            throw new RuntimeException("新增文化会议信息失败");
        }
        return cultureMeet.getId();
    }

    public List<Map<String, Object>> getLists(String title, Date beginningDate, Date endingDate) {
        List<CultureMeet> list = cultureMeetMapper.getList(title, beginningDate, endingDate);
        List<Map<String, Object>> cultureMeetList = new ArrayList<>();
        for (CultureMeet cultureMeet : list) {
            Map<String, Object> map = new HashMap<>();
            String pictureId = cultureMeet.getPictureId();
            List<Integer> numberList = new ArrayList<>();
            if (pictureId != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                    JsonNode rootNode = mapper.readTree(pictureId); // 解析字符串为JsonNode

                    // 遍历JsonNode中的元素，并将它们添加到List中
                    for (JsonNode node : rootNode) {
                        numberList.add(node.asInt()); // 将每个元素转换为Integer并添加到List中
                    }

                } catch (IOException e) {
                    e.printStackTrace(); // 处理任何可能发生的IOException
                }

                List<String> locationsList = new ArrayList<>();
                for (Integer i : numberList) {
                    String location = pictureMapper.getLocation(i);
                    locationsList.add(location);
                }
                map.put("locationsList", locationsList);
                map.put("cultureMeet", cultureMeet);
            } else {
                map.put("cultureMeet", cultureMeet);
                map.put("locationsList", null);
            }
            cultureMeetList.add(map);
        }
        return cultureMeetList;
    }

    public List<CultureMeet> list(String title, Date beginningDate, Date endingDate) {
        List<CultureMeet> list = cultureMeetMapper.getList(title, beginningDate, endingDate);
        return list;
    }

    public List<Map<String, Object>> getList(List<CultureMeet> list) {
        List<Map<String, Object>> cultureMeetList = new ArrayList<>();
        for (CultureMeet cultureMeet : list) {
            Map<String, Object> map = new HashMap<>();
            String pictureId = cultureMeet.getPictureId();
            List<Integer> numberList = new ArrayList<>();
            if (pictureId != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                    JsonNode rootNode = mapper.readTree(pictureId);
                    // 解析字符串为JsonNode
                    // 遍历JsonNode中的元素，并将它们添加到List中
                    for (JsonNode node : rootNode) {
                        numberList.add(node.asInt());
                        // 将每个元素转换为Integer并添加到List中
                    }
                } catch (IOException e) {
                    e.printStackTrace(); // 处理任何可能发生的IOException
                }
                List<String> locationsList = new ArrayList<>();
                for (Integer i : numberList) {
                    String location = pictureMapper.getLocation(i);
                    locationsList.add(location);
                }
                map.put("locationsList", locationsList);
                map.put("cultureMeet", cultureMeet);
            } else {
                map.put("cultureMeet", cultureMeet);
                map.put("locationsList", null);
            }
            cultureMeetList.add(map);
        }
        return cultureMeetList;
    }

    public AjaxResult getDetails(Integer id) {
        CultureMeet details = cultureMeetMapper.getDetails(id);
        AjaxResult ajax = AjaxResult.success();
        String pictureId = details.getPictureId();
        List<Integer> numberList = new ArrayList<>();
        if (pictureId != null) {
            try {
                ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                JsonNode rootNode = mapper.readTree(pictureId); // 解析字符串为JsonNode

                // 遍历JsonNode中的元素，并将它们添加到List中
                for (JsonNode node : rootNode) {
                    numberList.add(node.asInt()); // 将每个元素转换为Integer并添加到List中
                }

            } catch (IOException e) {
                e.printStackTrace(); // 处理任何可能发生的IOException
            }

            List<String> locationsList = new ArrayList<>();
            for (Integer i : numberList) {
                String location = pictureMapper.getLocation(i);
                locationsList.add(location);
            }
            ajax.put("cultureMeet", details);
            ajax.put("locationsList", locationsList);
        } else {
            ajax.put("cultureMeet", details);
            ajax.put("locationsList", null);
        }

        return ajax;
    }

    public void edit(CultureMeet cultureMeet) {
        int result = cultureMeetMapper.update(cultureMeet);
        if (result == 0) {
            throw new RuntimeException("修改文化会议信息失败");
        }
    }

    public void delete(Integer id) {
        int deleteResult = cultureMeetMapper.delete(id);
        if (deleteResult == 0) {
            throw new RuntimeException("删除文化会议信息失败");
        }
    }
}
