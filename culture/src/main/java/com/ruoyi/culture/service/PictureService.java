package com.ruoyi.culture.service;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.culture.domain.Picture;
import com.ruoyi.culture.mapper.PictureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PictureService {

    @Autowired
    PictureMapper pictureMapper;

    public void insert(Picture picture) {
        int insertResult = pictureMapper.insert(picture);
        if (insertResult==0){
            throw new RuntimeException("图片信息插入数据库失败");
        }
    }

    public AjaxResult getList(String type, Long typeFormId) {
        List<Picture> list = pictureMapper.getList(type, typeFormId);
        List<Long> pictureIds = list.stream().map(Picture::getId).collect(Collectors.toList());
        AjaxResult ajaxResult =new AjaxResult();
        ajaxResult.put("pictureIds",pictureIds);
        ajaxResult.put("Pictures",list);
        return ajaxResult;
    }

    public void delete(String type, Long typeFormId) {
        int delete = pictureMapper.delete(type, typeFormId);
        if (delete==0){
            throw new RuntimeException("图片信息删除数据库失败");
        }
    }
}
