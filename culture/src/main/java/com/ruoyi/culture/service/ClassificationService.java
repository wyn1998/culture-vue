package com.ruoyi.culture.service;


import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.culture.domain.Classification;
import com.ruoyi.culture.mapper.ClassificationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClassificationService {

    @Autowired
    ClassificationMapper classificationMapper;

    public void insert(Classification classification) {
        System.out.println("111"+classification);
        int parentId = classification.getParentId();
        Classification cf = classificationMapper.selectTypeById(parentId);
        classification.setAncestorId(cf.getAncestorId()+","+parentId);
        classificationMapper.addOne(classification);
    }


    /**
     * 构建前端所需要下拉树结构
     * @param classification 信息分类
     * @return 子节点
     */
    public List<Classification> buildDeptTree(List<Classification> classification)
    {
        List<Classification> returnList = new ArrayList<Classification>();
        List<Integer> tempList = classification.stream().map(Classification::getId).collect(Collectors.toList());
        for (Classification dept : classification)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId()))
            {
                recursionFn(classification, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = classification;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<Classification> list, Classification t)
    {
        // 得到子节点列表
        List<Classification> childList = getChildList(list, t);
        t.setChildren(childList);
        for (Classification tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }


    /**
     * 得到子节点列表
     */
    private List<Classification> getChildList(List<Classification> list, Classification t)
    {
        List<Classification> tlist = new ArrayList<Classification>();
        Iterator<Classification> it = list.iterator();
        while (it.hasNext())
        {
            Classification n = (Classification) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId() == t.getId())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }


    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<Classification> list, Classification t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }

    public List<Classification> list() {
        return classificationMapper.list();
    }

    /**
     * 更新分类
     * @param classification
     */
    public void update(Classification classification) {
        int parentId = classification.getParentId();
        Classification parentIdClassification = classificationMapper.selectTypeById(parentId);
        classification.setAncestorId(parentIdClassification.getAncestorId()+","+parentId);
        classification.setModifiedTime(new Date());
        int updateResult = classificationMapper.update(classification);
        if (updateResult==0){
            throw new RuntimeException("更新分类失败");
        }
    }

    /**
     * 删除分类
     * @param id
     */
    public void delete(int id) {
        int deleteResult = classificationMapper.delete(id);
        if (deleteResult==0){
            throw new RuntimeException("删除分类失败");
        }
    }

    public Classification getOne(Integer id) {
        return classificationMapper.getOne(id);
    }
}
