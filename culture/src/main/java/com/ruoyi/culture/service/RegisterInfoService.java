package com.ruoyi.culture.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.culture.domain.CreativePublish;
import com.ruoyi.culture.mapper.CreativePublishMapper;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.culture.domain.RegisterInfo;
import com.ruoyi.culture.domain.RegisterPart;
import com.ruoyi.culture.mapper.RegisterInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
public class RegisterInfoService {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    RegisterInfoMapper registerInfoMapper;

    @Autowired
    ISysUserService sysUserService;

    @Autowired
    CreativePublishMapper creativePublishMapper;

    @Transactional(rollbackFor = RuntimeException.class)
    public void insert(RegisterPart registerPart) {
        int i = registerInfoMapper.addOne(registerPart);
        if (i==0){
            throw new RuntimeException("注册信息添加失败");
        }
    }


    public RegisterInfo searchOne(String username) {
        SysUser user = sysUserService.selectUserByUserName(username);
        System.out.println("user:"+user);
        RegisterPart register = registerInfoMapper.search(username);
        System.out.println("register:"+register);
        RegisterInfo registerInfo=new RegisterInfo();
        try {
            BeanUtils.copyProperties(user,registerInfo);
            BeanUtils.copyProperties(register,registerInfo);
        } catch (BeansException e) {
            throw new RuntimeException(e);
        }
        return registerInfo;
    }

    public void update(RegisterPart registerPart) {
        System.out.println(registerPart);
        registerPart.setModifiedTime(new Date());
        SysUser sysUser =new SysUser();
        BeanUtils.copyProperties(registerPart,sysUser);
        SysUser user = sysUserService.selectUserByUserName(registerPart.getUserName());
        sysUser.setUserId(user.getUserId());
        int result = sysUserService.updateUser(sysUser);
        if (result==0){
            throw new RuntimeException("修改用户信息失败");
        }
        int updateResult = registerInfoMapper.update(registerPart);
        if (updateResult==0){
            throw new RuntimeException("修改用户信息失败");
        }
    }

    public List<CreativePublish> workList(Long userId) {
        List<CreativePublish> works = creativePublishMapper.getById(userId);
        return works;
    }

    @Transactional
    public Integer delete(String username) {
        int delete = registerInfoMapper.delete(username);
        int deleteUserByUserName = sysUserService.deleteUserByUserName(username);
        if (delete==0||deleteUserByUserName==0){
            throw new RuntimeException("用户删除信息失败");
        }
        return delete+deleteUserByUserName;
    }
}
