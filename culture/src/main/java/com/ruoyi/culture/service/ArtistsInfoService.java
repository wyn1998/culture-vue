package com.ruoyi.culture.service;


import com.ruoyi.culture.domain.ArtistsInfo;
import com.ruoyi.culture.mapper.ArtistsInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ArtistsInfoService {

    @Autowired
    ArtistsInfoMapper artistsInfoMapper;

//    private ArtistsInfoMapper artistsInfoMapper;
//
//    @Autowired
//    public void setArtistsInfoMapper(ArtistsInfoMapper artistsInfoMapper) {
//        this.artistsInfoMapper = artistsInfoMapper;
//    }

    public void insert(ArtistsInfo artistsInfo) {
        int insertResult = artistsInfoMapper.insert(artistsInfo);
        if (insertResult == 0) {
            throw new RuntimeException("新增艺术家失败");
        }
    }

    public List<ArtistsInfo> list() {
        List<ArtistsInfo> list = artistsInfoMapper.list();
        return list;
    }

    public void update(ArtistsInfo artistsInfo) {
        artistsInfo.setModifiedTime(new Date());
        int updateResult = artistsInfoMapper.update(artistsInfo);
        if (updateResult == 0) {
            throw new RuntimeException("修改艺术家失败");
        }
    }

    public void delete(int id) {
        int deleteResult = artistsInfoMapper.delete(id);
        if (deleteResult == 0) {
            throw new RuntimeException("删除艺术家失败");
        }
    }
}
