package com.ruoyi.culture.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.culture.domain.Classification;
import com.ruoyi.culture.domain.CreativePublish;
import com.ruoyi.culture.domain.CultureMeet;
import com.ruoyi.culture.mapper.CreativePublishMapper;
import com.ruoyi.culture.mapper.PictureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

import static com.ruoyi.common.utils.PageUtils.startPage;

@Service
public class CreativePublishService {

    @Autowired
    CreativePublishMapper creativePublishMapper;

    @Autowired
    PictureMapper pictureMapper;

    @Autowired
    ClassificationService classificationService;

    @Autowired
    LabelService labelService;

    public Integer insert(CreativePublish creativePublish) {
        int addResult = creativePublishMapper.addOne(creativePublish);
        if (addResult == 0) {
            throw new RuntimeException("新增信息发布失败");
        }
        return creativePublish.getId();
    }

    public List<CreativePublish> list(String title, Integer categoryId, Integer labelId, String type, Date beginningDate, Date endingDate) {
        List<CreativePublish> list = creativePublishMapper.list(title, categoryId, labelId, type, beginningDate, endingDate);
        return list;
    }

    public List<Map<String, Object>> getMaps(List<CreativePublish> list) {
        List<Map<String, Object>> creativePublishList = new ArrayList<>();
        for (CreativePublish creativePublish : list) {
            Map<String, Object> map = new HashMap<>();
            String pictureId = creativePublish.getPictureId();
            List<Integer> numberList = new ArrayList<>();
            String classificationName = classificationService.getOne(creativePublish.getCategoryId()).getClassificationName();
            String labelName = labelService.getOne(creativePublish.getLabelId()).getLabelName();
            if (pictureId != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                    JsonNode rootNode = mapper.readTree(pictureId); // 解析字符串为JsonNode

                    // 遍历JsonNode中的元素，并将它们添加到List中
                    for (JsonNode node : rootNode) {
                        numberList.add(node.asInt()); // 将每个元素转换为Integer并添加到List中
                    }

                } catch (IOException e) {
                    e.printStackTrace(); // 处理任何可能发生的IOException
                }

                List<String> locationsList = new ArrayList<>();
                for (Integer i : numberList) {
                    String location = pictureMapper.getLocation(i);
                    locationsList.add(location);
                }
                map.put("creativePublish", creativePublish);
                map.put("locationsList", locationsList);
                map.put("classificationName", classificationName);
                map.put("labelName", labelName);
            } else {
                map.put("creativePublish", creativePublish);
                map.put("locationsList", null);
                map.put("classificationName", classificationName);
                map.put("labelName", labelName);
            }


            creativePublishList.add(map);
        }
        return creativePublishList;
    }

    public void update(CreativePublish creativePublish) {
        int updateResult = creativePublishMapper.update(creativePublish);
        if (updateResult == 0) {
            throw new RuntimeException("更新发布的信息失败");
        }
    }

    public void delete(int id) {
        int deleteResult = creativePublishMapper.delete(id);
        if (deleteResult == 0) {
            throw new RuntimeException("删除发布的信息失败");
        }
    }

    public AjaxResult getDetails(Integer id, String type) {
        CreativePublish details = creativePublishMapper.getDetails(id, type);
        String classificationName = classificationService.getOne(details.getCategoryId()).getClassificationName();
        String labelName = labelService.getOne(details.getLabelId()).getLabelName();
        AjaxResult ajax = AjaxResult.success();
        String pictureId = details.getPictureId();
        List<Integer> numberList = new ArrayList<>();
        if (pictureId != null) {
            try {
                ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                JsonNode rootNode = mapper.readTree(pictureId); // 解析字符串为JsonNode

                // 遍历JsonNode中的元素，并将它们添加到List中
                for (JsonNode node : rootNode) {
                    numberList.add(node.asInt()); // 将每个元素转换为Integer并添加到List中
                }

            } catch (IOException e) {
                e.printStackTrace(); // 处理任何可能发生的IOException
            }

            List<String> locationsList = new ArrayList<>();
            for (Integer i : numberList) {
                String location = pictureMapper.getLocation(i);
                locationsList.add(location);
            }
            ajax.put("cultureMeet", details);
            ajax.put("locationsList", locationsList);
            ajax.put("classificationName", classificationName);
            ajax.put("labelName", labelName);
        } else {
            ajax.put("cultureMeet", details);
            ajax.put("locationsList", null);
            ajax.put("classificationName", classificationName);
            ajax.put("labelName", labelName);
        }
        return ajax;

    }

    public List<Map<String, Object>> getMap(String title, Integer categoryId, Integer labelId, String type, Date beginningDate, Date endingDate) {
        List<CreativePublish> list = creativePublishMapper.list(title, categoryId, labelId, type, beginningDate, endingDate);
        List<Map<String, Object>> creativePublishList = new ArrayList<>();
        for (CreativePublish creativePublish : list) {
            Map<String, Object> map = new HashMap<>();
            String pictureId = creativePublish.getPictureId();
            List<Integer> numberList = new ArrayList<>();
            String classificationName = classificationService.getOne(creativePublish.getCategoryId()).getClassificationName();
            String labelName = labelService.getOne(creativePublish.getLabelId()).getLabelName();
            if (pictureId != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper(); // 创建ObjectMapper实例
                    JsonNode rootNode = mapper.readTree(pictureId); // 解析字符串为JsonNode

                    // 遍历JsonNode中的元素，并将它们添加到List中
                    for (JsonNode node : rootNode) {
                        numberList.add(node.asInt()); // 将每个元素转换为Integer并添加到List中
                    }

                } catch (IOException e) {
                    e.printStackTrace(); // 处理任何可能发生的IOException
                }

                List<String> locationsList = new ArrayList<>();
                for (Integer i : numberList) {
                    String location = pictureMapper.getLocation(i);
                    locationsList.add(location);
                }
                map.put("creativePublish", creativePublish);
                map.put("locationsList", locationsList);
                map.put("classificationName", classificationName);
                map.put("labelName", labelName);
            } else {
                map.put("creativePublish", creativePublish);
                map.put("locationsList", null);
                map.put("classificationName", classificationName);
                map.put("labelName", labelName);
            }


            creativePublishList.add(map);
        }
        return creativePublishList;
    }
}
