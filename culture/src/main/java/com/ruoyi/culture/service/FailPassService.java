package com.ruoyi.culture.service;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.culture.domain.RegisterInfo;
import com.ruoyi.culture.mapper.RegisterInfoMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FailPassService {

    @Autowired
    RegisterInfoMapper registerInfoMapper;

    @Autowired
    SysUserMapper sysUserMapper;

    public List<RegisterInfo> getUserList(String identity,String username) {
        List<RegisterInfo> registerInfos = registerInfoMapper.failPassList(identity,username);
        return registerInfos;
    }

    public String selectUserNameByIdentity(String identity) {
        String username = registerInfoMapper.selectUserNameByIdentity(identity);
        String delFlag = sysUserMapper.getDelFlag(username);
        return delFlag;
    }

    public Integer updateFailPass(String username,String delFlag) {
        Integer updateResult = sysUserMapper.updateFailPass(username,delFlag);
        if (updateResult==0){
         throw new RuntimeException("用户后台审核成为会员失败");
        }
        return updateResult;
    }

    public boolean check(String username, String identity) {
        SysUser sysUser = sysUserMapper.checkUserNameUnique(username);
        if (StringUtils.isNotNull(sysUser)){
            throw new RuntimeException("用户名已存在，请重新设置");
        }
        String userResult = registerInfoMapper.selectUserNameByIdentity(identity);
        if (StringUtils.isNotNull(userResult)){
            throw new RuntimeException("身份证信息已存在，请重新设置");
        }
        return UserConstants.UNIQUE;
    }
}
