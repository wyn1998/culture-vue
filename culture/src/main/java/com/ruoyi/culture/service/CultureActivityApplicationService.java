package com.ruoyi.culture.service;


import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.culture.domain.CultureActivityApplication;
import com.ruoyi.culture.mapper.CultureActivityApplicationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CultureActivityApplicationService {

    @Autowired
    CultureActivityApplicationMapper cultureActivityApplicationMapper;

    public void signUp(CultureActivityApplication cultureActivityApplication) {
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        cultureActivityApplication.setUserId(sysUser.getUserId());
        cultureActivityApplicationMapper.insert(cultureActivityApplication);
    }
}
