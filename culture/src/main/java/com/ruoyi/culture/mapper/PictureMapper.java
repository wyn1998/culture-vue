package com.ruoyi.culture.mapper;

import com.ruoyi.culture.domain.Picture;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PictureMapper {


    int insert(Picture picture);

    List<Picture> getList(@Param("type") String type, @Param("typeFormId") Long typeFormId);

    String getLocation(@Param("id")Integer id);

    int delete(@Param("type") String type, @Param("typeFormId") Long typeFormId);
}
