package com.ruoyi.culture.mapper;


import com.ruoyi.culture.domain.CultureMeet;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Mapper
@Component
public interface CultureMeetMapper {
    int addOne(CultureMeet cultureMeet);

    List<CultureMeet> getList(@Param("title") String title,
                              @Param("beginningDate")Date beginningDate,
                              @Param("endingDate")Date endingDate);

    CultureMeet getDetails(@Param("id") Integer id);

    int update(CultureMeet cultureMeet);

    int delete(@Param("id")Integer id);
}
