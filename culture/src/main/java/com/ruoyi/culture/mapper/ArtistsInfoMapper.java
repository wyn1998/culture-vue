package com.ruoyi.culture.mapper;



import com.ruoyi.culture.domain.ArtistsInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface ArtistsInfoMapper {
    int insert(ArtistsInfo artistsInfo);

    List<ArtistsInfo> list();

    int update(ArtistsInfo artistsInfo);

    int delete(int id);
}
