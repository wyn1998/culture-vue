package com.ruoyi.culture.mapper;


import com.ruoyi.culture.domain.VoteInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface VoteInfoMapper {

    int insert(VoteInfo voteInfo);

    List<VoteInfo> list();

    int update(VoteInfo voteInfo);

    int delete(int id);

    Integer sum(Integer id);
}
