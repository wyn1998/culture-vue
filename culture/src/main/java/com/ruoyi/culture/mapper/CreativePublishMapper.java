package com.ruoyi.culture.mapper;


import com.ruoyi.culture.domain.CreativePublish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Mapper
@Component
public interface CreativePublishMapper {
    Integer addOne(CreativePublish creativePublish);

    List<CreativePublish> list(@Param("title") String title,
                               @Param("categoryId") Integer categoryId,
                               @Param("labelId")Integer labelId,
                               @Param("type")String type,
                               @Param("beginningDate") Date beginningDate,
                               @Param("endingDate") Date endingDate);

    int update(CreativePublish creativePublish);

    int delete(@Param("id") int id);

    List<CreativePublish> getById(Long id);

    CreativePublish getDetails(@Param("id") Integer id, @Param("type")String type);
}
