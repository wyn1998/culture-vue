package com.ruoyi.culture.mapper;


import com.ruoyi.culture.domain.Classification;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface ClassificationMapper {
    void addOne(Classification classification);

    Classification selectTypeById(int parentId);

    List<Classification> list();

    int update(Classification classification);

    int delete(int id);

    Classification getOne(Integer id);
}
