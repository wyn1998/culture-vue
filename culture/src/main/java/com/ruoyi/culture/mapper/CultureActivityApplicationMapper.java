package com.ruoyi.culture.mapper;

import com.ruoyi.culture.domain.CultureActivityApplication;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface CultureActivityApplicationMapper {
    int insert(CultureActivityApplication cultureActivityApplication);
}
