package com.ruoyi.culture.mapper;


import com.ruoyi.culture.domain.ExhibitionInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface ExhibitionInfoMapper {
    int insert(ExhibitionInfo exhibitionInfo);

    List<ExhibitionInfo> list();

    int update(ExhibitionInfo artistsInfo);

    int delete(int id);
}
