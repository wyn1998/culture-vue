package com.ruoyi.culture.mapper;



import com.ruoyi.culture.domain.RegisterInfo;
import com.ruoyi.culture.domain.RegisterPart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface RegisterInfoMapper {
    int addOne(RegisterPart registerPart);

    RegisterPart search(String username);

    int update(RegisterPart registerPart);

    List<RegisterInfo> failPassList(@Param("identity") String identity, @Param("username")String username);

    String selectUserNameByIdentity(String identity);

    int delete(String username);
}
