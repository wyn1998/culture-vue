package com.ruoyi.culture.mapper;


import com.ruoyi.culture.domain.Label;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface LabelMapper {
    int insert(Label label);

    List<Label> list();

    int update(Label label);

    int delete(int id);

    Label getOne(Integer id);
}
