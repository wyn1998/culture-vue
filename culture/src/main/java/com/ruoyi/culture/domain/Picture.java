package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Picture implements Serializable {

    @ApiModelProperty("id")
    @JsonProperty("id")
    @JsonIgnore
    private Long id;
    @ApiModelProperty("0为文创活动，1为文创会议，2为展览咨询")
    @JsonProperty("type")
    private String type;
    @ApiModelProperty("对应表格id")
    @JsonProperty("type_form_id")
    private Long typeFormId;
    @ApiModelProperty("图片地址")
    @JsonProperty("location")
    private String location;
    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonIgnore
    private String createTime;
    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonIgnore
    private String modifiedTime;
    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    @JsonIgnore
    private String reversed;

}
