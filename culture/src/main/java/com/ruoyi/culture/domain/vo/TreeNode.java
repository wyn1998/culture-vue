package com.ruoyi.culture.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.culture.domain.Classification;


import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class TreeNode implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private Long id;

    /** 节点名称 */
    private String label;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeNode> children;

    public TreeNode()
    {

    }

    public TreeNode(Classification classification)
    {
        this.id = Long.valueOf(classification.getId());
        this.label = classification.getClassificationName();
        this.children = classification.getChildren().stream().map(TreeNode::new).collect(Collectors.toList());
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public List<TreeNode> getChildren()
    {
        return children;
    }

    public void setChildren(List<TreeNode> children)
    {
        this.children = children;
    }
}
