package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@ApiModel("展览信息表")
@Data
public class ExhibitionInfo implements Serializable {

    @ApiModelProperty("报名id")
    @JsonProperty("id")
    private int id;

    @ApiModelProperty("标题")
    @JsonProperty("title")
    private String title;

    @ApiModelProperty("内容")
    @JsonProperty("content")
    private String content;

    @ApiModelProperty("所属机构")
    @JsonProperty("organization")
    private String organization;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    @JsonIgnore
    private String reversed;

    @ApiModelProperty("图片表id")
    @JsonProperty("picture_id")
    private String pictureId;

}
