package com.ruoyi.culture.domain.works;

import lombok.Data;

@Data
public class Exhibition {

    private String title;
}
