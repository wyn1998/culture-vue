package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@ApiModel("投票信息")
@Data
public class VoteInfo implements Serializable {


    @ApiModelProperty("id")
    @JsonProperty("id")
    private Long id;
    @ApiModelProperty("投票对象id")
    @JsonProperty("vote_obj_id")
    private Long voteObjId;
    @ApiModelProperty("用户id")
    @JsonProperty("user_id")
    private Long userId;
    @ApiModelProperty("投票对象名称")
    @JsonProperty("vote_obj_name")
    private String voteObjName;
    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;
    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    private String reversed;

}
