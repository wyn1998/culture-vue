package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@ApiModel("文创活动报名")
@Data
public class CultureActivityApplication implements Serializable {



    @ApiModelProperty("报名id")
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty("用户id")
    @JsonProperty("user_id")
    private Long userId;

    @ApiModelProperty("信息发布id")
    @JsonProperty("creative_publish_id")
    private Long creativePublishId;

    @ApiModelProperty("活动时间")
    @JsonProperty("join_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date joinTime;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    private String reversed;

}
