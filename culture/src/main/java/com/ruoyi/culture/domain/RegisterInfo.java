package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@ApiModel("用户注册信息表")
@Data
public class RegisterInfo extends SysUser implements Serializable{

    /** 用户ID */
    @ApiModelProperty("userId")
    @JsonProperty("user_id")
    private Long userId;

    @ApiModelProperty("id")
    @JsonProperty("id")
    private int id;

    @ApiModelProperty("身份证号码")
    @JsonProperty("identity")
    private String identity;

    @ApiModelProperty("注册类型")
    @JsonProperty("register_type")
    private int registerType;

    @ApiModelProperty("住址")
    @JsonProperty("address")
    private String address;

    @ApiModelProperty("企业有效营业执照")
    @JsonProperty("license")
    private String license;

    @ApiModelProperty("企业信息")
    @JsonProperty("enterprise_info")
    private String enterpriseInfo;

    @ApiModelProperty("法人信息")
    @JsonProperty("legal_info")
    private String legalInfo;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    private String reversed;

}
