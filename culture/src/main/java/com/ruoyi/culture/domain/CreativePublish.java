package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@ApiModel("文创信息发布表")
@Data
public class CreativePublish implements Serializable {


    @ApiModelProperty("id")
    @JsonProperty("id")
    private int id;

    @ApiModelProperty("分类id")
    @JsonProperty("category_id")
    private int categoryId;

    @ApiModelProperty("标签id")
    @JsonProperty("label_id")
    private int labelId;


    @ApiModelProperty("标题")
    @JsonProperty("title")
    private String title;

    @ApiModelProperty("内容")
    @JsonProperty("content")
    private String content;

    @ApiModelProperty("附件地址")
    @JsonProperty("location")
    private String location;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    private String reversed;

    //新加字段
    @ApiModelProperty("0为文创作品，2为活动报名")
    @JsonProperty("type")
    private String type;

    @ApiModelProperty("用户id")
    @JsonProperty("user_id")
    private Long userId;

    @ApiModelProperty("图片表id")
    @JsonProperty("picture_id")
    private String pictureId;

}
