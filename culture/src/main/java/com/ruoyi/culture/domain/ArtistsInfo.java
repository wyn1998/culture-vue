package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@ApiModel("艺术家信息管理")
@Data
public class ArtistsInfo implements Serializable {


    @ApiModelProperty("id")
    @JsonProperty("id")
    private int id;

    @ApiModelProperty("艺术家姓名")
    @JsonProperty("artists_name")
    private String artistsName;

    @ApiModelProperty("简介")
    @JsonProperty("artists_content")
    private String artistsContent;

    @ApiModelProperty("作品链接")
    @JsonProperty("work_link")
    private String workLink;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    private String reversed;
}
