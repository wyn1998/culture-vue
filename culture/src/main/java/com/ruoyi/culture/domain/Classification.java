package com.ruoyi.culture.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ApiModel("信息发布分类表")
@Data
public class Classification implements Serializable {


    @ApiModelProperty("id")
    @JsonProperty("id")
    private int id;

    @ApiModelProperty("父级id")
    @JsonProperty("parent_id")
    private int parentId;

    @ApiModelProperty("祖级id")
    @JsonProperty("ancestor_id")
    private String ancestorId;

    @ApiModelProperty("分类名称")
    @JsonProperty("classification_name")
    private String classificationName;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("修改时间")
    @JsonProperty("modified_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifiedTime;

    @ApiModelProperty("保留字段")
    @JsonProperty("Reversed")
    private String reversed;


    @Setter
    @Getter
    private List<Classification> children = new ArrayList<Classification>();
}
